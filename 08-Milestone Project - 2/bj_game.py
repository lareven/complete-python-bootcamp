# Define global variables
import random
# from IPython.display import clear_output
# from bj_classes import *

suits = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
ranks = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace')
values = {'Two':2, 'Three':3, 'Four':4, 'Five':5, 'Six':6, 'Seven':7, 'Eight':8, 'Nine':9, 'Ten':10, 'Jack':10,
         'Queen':10, 'King':10, 'Ace':11}


# Class definition
class Card:
    '''
    a Card object. Stores the suit and rank of a card
    Input: suit, rank
    '''
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank
    
    def __str__(self):
        return f'{self.rank} of {self.suit}'
    

    
class Deck:
    '''
    a Deck object. 
    Input: None
    Methods: shuffle, deal
    '''
    def __init__(self):
        self.deck = []  # start with an empty list
        for suit in suits:
            for rank in ranks:
                self.deck.append(Card(suit, rank))
    
    def __str__(self):
        return ', '.join(card.__str__() for card in self.deck[-6:])

    def shuffle(self):
        random.shuffle(self.deck)
        
    def deal(self):
        dealed_card = self.deck.pop()
        return dealed_card
    
    
    
class Hand:
    '''
    a Hand object. Tracks the player and dealer's hand during a game
    Input: None
    Methods: add_card, adjust_for_ace
    '''
    def __init__(self):
        self.cards = []  # start with an empty list as we did in the Deck class
        self.value = 0   # start with zero value
        self.vis_value = 0
        self.aces = 0    # add an attribute to keep track of aces
    
    def add_card(self,card):
        self.cards.append(card)
        self.value += values[card.rank]
        
        if len(self.cards) > 1:
            self.vis_value += values[card.rank]
            
        if card.rank == 'Ace':
            self.aces += 1
    
    def adjust_for_ace(self):
        while self.aces and self.value > 21:
            self.value -= 10
            self.aces -= 1           
            
            

class Chips:
    '''
    a Chips class. Stores and tracks the player's chips throughout all games
    Input: total (optional), bet (optional)
    Methods: set_bet, win_bet, lose_bet
    '''
    def __init__(self, total=100, bet=0):
        self.total = total  # This can be set to a default value or supplied by a user input
        self.bet = bet
        
    def set_bet(self, bet):
        self.bet = bet
        
    def win_bet(self):
        self.total += self.bet
    
    def lose_bet(self):
        self.total -= self.bet
        
        
        
# Function definition
def take_bet(player_chips):
    '''
    function to ask player for a bet size. also check if total chips cover the bet size
    Input: Chips object
    '''
    while True:
        try:
            bet = int(input('How much do you want to bet? '))
        except:
            print('Not a valid number. Try again!\n')
        else:
            if bet > player_chips.total:
                print('Not enough chips!\n')
            else:
                player_chips.set_bet(bet)
                break

                
def hit(deck,hand):
    '''
    Function to deal the player a card if chooses to hit
    Input: Deck object, player's Hand object
    '''
    # new_card = deck.deal()
    hand.add_card(deck.deal())
    hand.adjust_for_ace()
    
    
def hit_or_stand(deck,hand):
    '''
    Function to ask player to hit or stand. If hit, call hit(); if stand, turn off global variable "playing"
    Input: Deck object, player's Hand object
    '''
    global playing  # to control an upcoming while loop
    
    while True:
        action = input('Do you want to hit or stand? ')
        if action[0].lower() == 'h':
            hit(deck, hand)
            return False
        elif action[0].lower() == 's':
            playing = False
            return True
        else:
            print('I do not understand. Try again!')
            
            

# improve by passing * to print       
def show_some(player,dealer):
    '''
    pass player and dealer's hand
    print card in hand and total value, hide first card of dealer
    '''
    print("PLayer's cards are: ")
    for card in player.cards:
        print(f'\t{card}')
    print(f"Player's card value is {player.value}\n")

    print("Dealer's cards are: ")
    print(" <card hidden>")
    for card in dealer.cards[1:]:
        print(f'\t{card}')
    print(f"Dealer's visible card value is {dealer.vis_value}\n")

    
# improve by passing * to print       
def show_all(player,dealer):
    '''
    pass player and dealer's hand
    print card in hand and total value
    '''
    print("PLayer's cards are: ")
    for card in player.cards:
        print(f'\t{card}')
    print(f"Player's card value is {player.value}\n")

    print("Dealer's cards are: ")
    for card in dealer.cards:
        print(f'\t{card}')
    print(f"Dealer's card value is {dealer.value}\n")
    
    
# a series of actions depends on the winning scenario    
def player_busts(player_chip):
    global playing

    playing = False
    player_chip.lose_bet()
    
    print('You are busted!')
    print(f'Your remaining chips are {player_chip.total}')
    

def player_wins(player_chip):
    player_chip.win_bet()
    print('You win!')

def dealer_busts(player_chip):
    player_chip.win_bet()
    print('Dealer is busted. You win!')
    

def dealer_wins(player_chip):
    player_chip.lose_bet()
    print('Dealer wins')
    
def push():
    print('It is a push')
    
def player_bj(player_chip):
    global playing

    playing = False
    player_chip.win_bet()

    print('You have got Black Jack! You win!')
    
def dealer_bj(player_chip):
    player_chip.lose_bet()
    print('The dealer has got Black Jack! Dealer wins!')    

    
    
# Game on!
playing = True


# Set up the Player's chips 
player_chips = Chips()

while True:
    # Print an opening statement
#     clear_output()
    print('Welcome to Black Jack!')
    
    # Create & shuffle the deck, deal two cards to each player
    new_deck = Deck()
    new_deck.shuffle()
    
    player_hand = Hand()
    dealer_hand = Hand()
    
    for _ in range(2):
        player_hand.add_card(new_deck.deal())
        dealer_hand.add_card(new_deck.deal())
    
    player_hand.adjust_for_ace()
    dealer_hand.adjust_for_ace()
        
    
    # Prompt the Player for their bet
    take_bet(player_chips)
    print(f'Your total chips are {player_chips.total}')
    
    # Show cards (but keep one dealer card hidden)
    print('\n')
    show_some(player_hand, dealer_hand)
    
    # If player gets a black jack
    if player_hand.value == 21:
        player_bj(player_chips)
    else:
    
        while playing:  # recall this variable from our hit_or_stand function

            # Prompt for Player to Hit or Stand
            stand = hit_or_stand(new_deck, player_hand)

            # Show cards (but keep one dealer card hidden)
            if not stand:
                show_some(player_hand, dealer_hand)

            # If player's hand exceeds 21, run player_busts() and break out of loop
            if player_hand.value > 21:
                player_busts(player_chips)
            # If player's hand equal to 21, run player_bj and break out of loop
            elif player_hand.value == 21:
                player_bj(player_chips)
            
        if stand:                
            # If Player hasn't busted, play Dealer's hand until Dealer reaches 17
            print('\nDealer hand playing...')
            while dealer_hand.value < 17:
                # dealer_hand.add_card(new_deck.deal())
                # dealer_hand.adjust_for_ace()
                hit(new_deck,dealer_hand)

            # Show all cards
            show_all(player_hand, dealer_hand)

            # Run different winning scenarios
            if dealer_hand.value > 21:
                dealer_busts(player_chips)
            elif dealer_hand.value == 21:
                dealer_bj(player_chips)
            elif dealer_hand.value > player_hand.value:
                dealer_wins(player_chips)
            elif dealer_hand.value < player_hand.value:
                player_wins(player_chips)
            else:
                push()
        
    
    # Inform Player of their chips total 
    print(f'Your current total chips are {player_chips.total}')
    
    # Ask to play again
    while True:
        play_again = input('Do you have to play again? (yes/no) ')

        if play_again[0].lower() == 'y':
            playing = True
            break
        elif play_again[0].lower() == 'n':
            break
        else:
            print("I don't understand. try again.")
            continue
    
    if play_again[0].lower() == 'n':
        print('Thank you for playing!')
        break