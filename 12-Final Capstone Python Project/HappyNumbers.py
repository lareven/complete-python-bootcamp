'''
This program does the following transformation on a number:
Sum of the squares all the digits of a number. If the number ends up to be 1, it is a happy number.
Input: n
Output: first n happy numbers
'''

import sys

def find_happy(number):
    '''
    Boolean function
    Input: positive integer
    Output: whether this number is a happy number

    Mapping of single digits: {1:1,2:6,3:1,4:6,5:5,6:6,7:1,8:6,9:1}
    '''
    while number > 10:
        digits = find_digit(number) 
        number = sum(map(pow, digits, [2]*len(digits)))
    
    if number in [1,3,7,9]:
        return True
    else:
        return False


def find_digit(number):
    '''
    Input: number
    Output: all digits of the number
    '''
    digits = []

    while number > 0:
        digit = number%10
        number = number//10
        
        digits.append(digit)

    return digits

if __name__ == '__main__':
    try:
        n = int(sys.argv[1]) # argument passed from the command line
    except:
        n = 8

    print('Are you happy?')

    number = 1
    while True:
        if find_happy(number):
            print(number)
            n -= 1
        if n == 0:
            break
        number += 1
